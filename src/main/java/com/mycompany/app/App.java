package com.mycompany.app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	List<Device> device = new ArrayList<>();
        String SQL_SELECT = "Select * from Device";
        Connection conn=null;
        ConnectionDB conexion = new ConnectionDB();
        conn = conexion.Conexion();
        
    	if(conn!=null) {
            System.out.println("Connected to the database!");
            
            PreparedStatement preparedStatement;
			try {
				preparedStatement = conn.prepareStatement(SQL_SELECT);
                ResultSet resultSet = preparedStatement.executeQuery();
                while(resultSet.next()) {
            	 long deviceId=resultSet.getLong("deviceId");
            	 String name= resultSet.getString("name");
            	 String description= resultSet.getString("description");
            	 long manufacturerId= resultSet.getLong("manufacturerId");
            	 long colorId = resultSet.getLong("colorId");
            	 String comments = resultSet.getString("comments");
            	 Device dev = new Device(deviceId,name,description,manufacturerId,colorId,comments);
            	 device.add(dev);
                }
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}else {
    		System.out.println("Conexion no establecida");
    	}
    	device.forEach(System.out::println);
    	
    	List<String> list = device.stream()
        		.map((deviceName)->deviceName.getName()).filter((name)->(Pattern.compile("Laptop")).matcher(name).matches())
        		.collect(Collectors.toList());
        		//.forEach(System.out::println);
        	System.out.println("LISTA");
        	list.forEach(System.out::println);
        	
        	long coun = device.stream()
            		.filter((deviceid)->deviceid.getManufacturerId() == 3)
            		.count();
            	System.out.println("Las coincidecias fueron: "+coun);
            	
            	List<Device> deviceColorId = device.stream()
            			.filter((deviceCol)-> deviceCol.getColorId() == 1)
            			.collect(Collectors.toList());
            	
            	deviceColorId.forEach(System.out::println);
            	
            	Map<Object, Object> mapa =device.stream()
            			.collect(Collectors.toMap(
            					v-> v.getDeviceId(),
            					v-> v
            			));
            	System.out.println("MAPA");
            	mapa.forEach((key, value)-> System.out.println(key + ":" + value));
    }
}
